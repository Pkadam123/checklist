import React from 'react'
import { Logged } from '../utils/logic'
import { Navigate, Outlet } from 'react-router-dom'
import { useContext } from 'react'
import { loginContext } from '../context/LoginContext'

function PublicRoute() {
    const { loggedin } = useContext(loginContext)
    if (!Logged() || !loggedin) {
        return <Outlet />
    } else {
        return <Navigate to="/dashboard" />
    }
    return (
        <div>PublicRoute</div>
    )
}

export default PublicRoute