import React from 'react'
import { Logged } from '../utils/logic'
import { Navigate, Outlet } from 'react-router-dom'
import { useContext } from 'react'
import { loginContext } from '../context/LoginContext'

const PrivateRoute = () => {
    const { loggedin } = useContext(loginContext)
    if (Logged() || loggedin) {
        return <Outlet />
    } else {
        return <Navigate to="/login" />
    }
    return (
        <div>PrivateRoute</div>
    )
}

export default PrivateRoute