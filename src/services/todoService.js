import axios from "axios"
export const get_todo = async () => {
    try {
        let data = await axios.get(import.meta.env.VITE_TODO_URL)
        return data?.data
    } catch (err) {
        console.log(err)
    }
}

export const update_todo = async (id, body) => {
    try {
        let data = await axios.put(`${import.meta.env.VITE_TODO_URL}/${id}`, body)
        return data?.data
    } catch (err) {
        console.log(err)
    }
}
export const add_todo = async (val) => {
    try {
        let data = await axios.post(`${import.meta.env.VITE_TODO_URL}`, val)
        return data?.data
    } catch (err) {
        console.log(err)
    }
}
export const delete_todo = async (id) => {
    try {
        let data = await axios.delete(`${import.meta.env.VITE_TODO_URL}/${id}`)
        return data?.data
    } catch (err) {
        console.log(err)
    }
}