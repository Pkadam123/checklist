import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { add_todo, delete_todo, get_todo, update_todo } from '../services/todoService'
import { useDispatch } from 'react-redux'
const initialState = {
    todo: [],
    fil: [],
    pending: false,
    load: false
}
export const todoSlice = createSlice({
    name: 'todo',
    initialState,
    reducers: {
    },

    extraReducers: (builder) => {
        builder.addCase(gettodo.fulfilled, (state, action) => {
            state.todo = action.payload
            state.load = false
            state.error = false
        }).addCase(updatetodo.fulfilled, (state, action) => {
            // state.todo = action.payload
            state.load = false
            state.error = false
        }).addCase(deletetodo.fulfilled, (state, action) => {
            // state.todo = action.payload
            state.load = false
            state.error = false
        })
            .addCase(gettodo.pending, (state, action) => {
                state.load = true
                state.error = false
            }).addCase(gettodo.rejected, (state, action) => {
                state.load = false
                state.error = true
            }).addCase(updatetodo.pending, (state, action) => {
                state.load = true
                state.error = false
            }).addCase(updatetodo.rejected, (state, action) => {
                state.load = false
                state.error = true
            })
            .addCase(deletetodo.pending, (state, action) => {
                state.load = true
                state.error = false
            }).addCase(deletetodo.rejected, (state, action) => {
                state.load = false
                state.error = true
            })

    },
})
// export const addburger = createAsyncThunk(
//     'burger/add',
//     async (thunkAPI) => {
//         console.log(thunkAPI)
//         const data = await axios.post("http://localhost:8090/addProduct", thunkAPI, { withCredentials: true })
//         return data.data.user

//     }
// )
export const gettodo = createAsyncThunk(
    'todo/get',
    async (thunkAPI) => {
        let data = await get_todo()
        return data
    }
)
export const addtodo = createAsyncThunk(
    'todo/add',
    async (thunkAPI) => {
        // const { id, value } = thunkAPI
        let data = await add_todo(thunkAPI)
        // return data
    }
)
export const updatetodo = createAsyncThunk(
    'todo/update',
    async (thunkAPI) => {
        const { id, value } = thunkAPI
        let data = await update_todo(id, value)
        // return data
    }
)
export const deletetodo = createAsyncThunk(
    'todo/delete',
    async (thunkAPI) => {
        let data = await delete_todo(thunkAPI)
        // return data
    }
)
// Action creators are generated for each case reducer function
export const { setSearch } = todoSlice.actions
export default todoSlice.reducer