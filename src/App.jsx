import './App.css'
import { Routes, Route } from "react-router-dom"
import PrivateRoute from './routes/PrivateRoute'
import { lazy, useEffect } from 'react'
import PublicRoute from './routes/PublicRoute'
const LoginLazy = lazy(() => import("./components/page/login/Login"))
const HomeLazy = lazy(() => import("./components/page/HomeLayout/HomeLayout"))
const DashLazy = lazy(() => import("./components/page/dashboard/Dashboard"))
const TimerLazy = lazy(() => import("./components/page/timer/Timer"))
const TodoLazy = lazy(() => import("./components/page/todo/Todo"))
function App() {
  return (
    <>
      <Routes>
        <Route element={<PublicRoute />}>
          <Route path='/login' element={<LoginLazy />} />
        </Route>
        <Route element={<PrivateRoute />}>
          <Route path='/' element={<HomeLazy />}>
            <Route path='/dashboard' element={<DashLazy />} />
            <Route path='/todo' element={<TodoLazy />} />
            <Route path='/timer' element={<TimerLazy />} />
          </Route>
        </Route>
      </Routes>
    </>
  )
}

export default App
