import React from 'react'
import styles from "./InputField.module.scss"
import SHOW from "../../../assets/icons/png/view.png"
import HIDE from "../../../assets/icons/png/hide.png"
import { useContext } from 'react'
import { loginContext } from '../../../context/LoginContext'
const InputField = (props) => {
    const { show, toggleShow } = useContext(loginContext)
    return (
        <div className={styles?.input_container}>
            <label>{props?.label}:</label>
            <input
                className={styles?.input}
                {
                ...props?.register
                }
                type={props?.type}
                {...props}
            />
            <div className={styles?.eye_icon}>
                {
                    props?.type == "password" ? <img src={SHOW} onClick={() => toggleShow()} /> : props?.type == "text" ? <img src={HIDE} onClick={() => toggleShow()} /> : ""
                }
            </div>
            <p className={styles?.input_error}>
                {props?.errors}
            </p>
        </div>
    )
}

export default InputField