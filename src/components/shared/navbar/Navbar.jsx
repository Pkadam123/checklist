import React, { useState } from 'react'
import styles from "./Navbar.module.scss"
import { useContext } from 'react'
import { loginContext } from '../../../context/LoginContext'
import { Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { setSearch } from '../../../redux/todoSlice'
const Navbar = () => {
    const { toggle } = useContext(loginContext)

    return (
        <div>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div class="container-fluid">
                    {/* <a class="navbar-brand" href="#">Checklist</a> */}
                    <Link to="/dashboard"><button className='btn' style={{ color: "white" }}>Checklist</button></Link>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <button className='btn' onClick={() => {
                                    localStorage.removeItem("token"),
                                        toggle()
                                }} style={{ color: "white" }}>Logout</button>
                            </li>
                            {/* <li class="nav-item">
                                <a class="nav-link" href="#">Link</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Dropdown
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                    <li><hr class="dropdown-divider" /></li>
                                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled">Disabled</a>
                            </li> */}
                        </ul>
                        {/* <form class="d-flex" onSubmit={handleSubmit}>
                            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" name='search' onChange={(e) => setValue(e.target.value)} value={value} />
                            <button class="btn btn-outline-success" type="submit">Search</button>
                        </form> */}
                    </div>
                </div>
            </nav>

        </div>
    )
}

export default Navbar