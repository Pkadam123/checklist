import React from 'react'
import styles from "./Modal.module.scss"
import InputField from '../inputfield/InputField'
import Button from '../button/Button'
import { useDispatch } from 'react-redux'
import { useForm } from "react-hook-form"
import { addtodo, deletetodo, gettodo, updatetodo } from '../../../redux/todoSlice'
const Modal = (props) => {
    const dispatch = useDispatch()
    const { register, handleSubmit, formState: { errors } } = useForm({
        defaultValues: {
            name: props?.selectedData?.name || "",
            avatar: props?.selectedData?.avatar || "",
            // phone: props?.selectedData?.phone || ""
        }
    })
    const submit = (val) => {
        console.log(val)
        Object.values(props?.selectedData)?.length == 0
            ?
            dispatch(addtodo(val)
            ).then(() => {
                dispatch(gettodo())
                props?.setModal(false)
            })
            :
            dispatch(updatetodo({
                id: props?.selectedData?.id,
                value: val
            })
            ).then(() => {
                dispatch(gettodo())
            })
        props?.setModal(false)
        props?.setSelectedData({})
    }
    const handleDelete = () => {
        dispatch(deletetodo(props?.selectedData?.id)).then(() => {
            dispatch(gettodo())
        })
        props?.setDeleteModal(false)
        props?.setSelectedData({})
    }
    return (
        <div>
            <div className={styles?.modal_body}>

                <div className='row mt-5'>
                    <div className='col-4 mx-auto bg-white rounded-5 p-5'>
                        {
                            props?.delete ?
                                <>
                                    <div className='d-flex flex-column p-5'>
                                        <p>Are You Sure You Want To Delete??</p>
                                        <div className='d-flex gap-4'>
                                            <Button
                                                label="Cancel"
                                                theme="primary"
                                                onClick={() => {
                                                    props?.setDeleteModal(false)
                                                    props?.setSelectedData({})
                                                }}
                                            />
                                            <Button
                                                label="Delete"
                                                theme="danger"
                                                onClick={handleDelete}
                                            />
                                        </div>
                                    </div>
                                </>
                                :
                                <form onSubmit={handleSubmit(submit)}>
                                    <InputField
                                        label="Name"
                                        register={register("name", {
                                            required: {
                                                value: true,
                                                message: "This Field is Required"
                                            }
                                        })}

                                        errors={errors?.name ? errors?.name?.message : ""}
                                    />
                                    <InputField
                                        label="Avatar"
                                        register={register("avatar", {
                                            required: {
                                                value: true,
                                                message: "This Field is Required"
                                            }
                                        })}

                                        errors={errors?.avatar ? errors?.avatar?.message : ""}
                                    />
                                    {/* <InputField
                                        label="Phone"
                                        register={register("phone", {
                                            required: {
                                                value: true,
                                                message: "This Field is Required"
                                            }
                                        })}
                                        errors={errors?.phone ? errors?.phone?.message : ""}
                                    /> */}
                                    <Button
                                        theme="primary"
                                        label={Object.values(props?.selectedData)?.length == 0 ? "Add" : "Edit"}
                                        type="submit"
                                    // onClick={handleEdit} 
                                    />
                                    <Button
                                        theme="danger"
                                        label="Cancel"
                                        onClick={() => {
                                            props?.setModal(false),
                                                props?.setSelectedData({})
                                        }

                                        } />
                                </form>
                        }

                    </div>

                </div>
            </div>

        </div>
    )
}

export default Modal