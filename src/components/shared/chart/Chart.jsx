import React from 'react'
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Doughnut } from "react-chartjs-2";
import { topics } from '../../../utils/logic';
const Chart = () => {
    ChartJS.register(ArcElement, Tooltip, Legend);

    const key = Object.keys(topics())
    const value = Object?.values(topics())
    const generateColorCodes = (length) => {
        const colorCodes = [];
        for (let i = 0; i < length; i++) {
            const randomColor = `#${Math.floor(Math.random() * 16777215).toString(16)}`;
            colorCodes.push(randomColor);
        }
        return colorCodes;
    };
    const backgroundColors = generateColorCodes(value.length);
    const data = {
        labels: key,
        datasets: [
            {
                data: value, // Numeric data for each segment
                backgroundColor: backgroundColors // Background color for each segment
            },
        ],
    };
    return (
        <div>
            <Doughnut data={
                data
            } />
        </div>
    )
}

export default Chart