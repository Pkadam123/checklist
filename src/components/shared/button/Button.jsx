import React from 'react'

const Button = (props) => {
    return (
        <div>
            <button
                className={`btn btn-${props?.theme}`}
                {...props}>
                {
                    props?.label
                }

            </button>


        </div>
    )
}

export default Button