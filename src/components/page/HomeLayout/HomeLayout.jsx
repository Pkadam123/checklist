import React from 'react'
import Navbar from '../../shared/navbar/Navbar'
import { Link, Outlet } from 'react-router-dom'
const HomeLayout = () => {
    return (
        <div>
            <Navbar />
            <Outlet />
        </div>
    )
}

export default HomeLayout