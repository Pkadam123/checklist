import React from 'react'
import { Link } from 'react-router-dom'
import Chart from "../../shared/chart/Chart"
import styles from "./Dashboard.module.scss"
const Dashboard = () => {
    return (
        <div>
            <div className='container-fluid'>
                <div className='row mt-5'>
                    <div className='col-6'>
                        <Chart />
                    </div>
                    <div className='col-6'>
                        <div className={styles?.content}>
                            <ul>
                                <li>
                                    <Link to="/todo">Todo List</Link>
                                </li>
                                <li>
                                    <Link to="/timer">Stop Watch</Link>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default Dashboard