import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { gettodo } from '../../../redux/todoSlice'
import Table from '../../shared/table/Table'
import Button from '../../shared/button/Button'
import Loader from '../../shared/loader/Loader'
import Modal from '../../shared/modal/Modal'
const Todo = () => {
    const dispatch = useDispatch()
    const [modal, setModal] = useState(false)
    const [deleteModal, setDeleteModal] = useState(false)
    const [selectedData, setSelectedData] = useState({})
    const { todo, load, fil } = useSelector((state) => state?.todo)
    const handleEdit = (data) => {
        setModal(true)
        setSelectedData(data)
    }
    const handleDelete = (data) => {
        setDeleteModal(true)
        setSelectedData(data)
    }
    const columns = [
        {
            name: 'Name',
            selector: row => row.name,
        },
        {
            name: 'Avatar',
            selector: row => row.avatar,
        },
        // {
        //     name: 'Phone',
        //     selector: row => row.phone,
        // },
        {
            name: "Actions",
            cell: (row) => (
                <div className='d-flex gap-3'>
                    <Button theme="primary" label="Edit" onClick={() => handleEdit(row)} />
                    <Button theme="danger" label="Delete" onClick={() => handleDelete(row)} />
                </div>
            )
        }
    ]
    useEffect(() => {
        dispatch(gettodo())
    }, [])
    console.log('todo', todo)
    console.log('fil', fil)
    return (
        <div>
            {
                load ? <Loader /> : <>
                    <Button label="Add"
                        theme="success"
                        onClick={() => setModal(true)}
                    />
                    <Table
                        columns={
                            columns
                        }
                        data={
                            todo
                        }
                    />
                    {
                        modal && <Modal
                            selectedData={selectedData}
                            setSelectedData={setSelectedData}
                            setModal={setModal}
                        />
                    }
                    {
                        deleteModal && <Modal
                            selectedData={selectedData}
                            setSelectedData={setSelectedData}
                            setDeleteModal={setDeleteModal}
                            delete={true}
                        />
                    }
                </>

            }

        </div>
    )
}

export default Todo