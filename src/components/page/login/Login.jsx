import React from 'react'
import { useContext } from 'react'
import { loginContext } from '../../../context/LoginContext'
import Button from '../../shared/button/Button'
import { useForm } from "react-hook-form"
import InputField from '../../shared/inputfield/InputField'
import { PASSWORD_REGEX } from '../../../utils/regex'
import styles from "./Login.module.scss"
const Login = () => {
    const { toggle, show } = useContext(loginContext)
    const { register, handleSubmit, formState: { errors }, setError } = useForm({
        defaultValues: {
            email: "",
            password: ""
        }
    })
    const submit = (values) => {
        const { email, password } = values
        if (email && password) {
            localStorage.setItem("token", "dsjkhfufhsdfh"),
                toggle()
        }
    }
    return (
        <div>
            <form onSubmit={handleSubmit(submit)} className={styles?.form}>

                <InputField
                    label="Email"
                    type="email"
                    register={register('email', {
                        required: { value: true, message: 'This field is required' },
                    })}
                    errors={errors?.email ? errors?.email?.message : ""}
                />
                <InputField
                    label="Password"
                    type={!show ? "password" : "text"}
                    register={register('password', {
                        required: { value: true, message: 'This field is required' },
                        validate: (password) => {
                            if (!PASSWORD_REGEX.test(password)) {
                                return 'Invalid password format'
                            }
                            return true
                        }
                    })}
                    errors={errors?.password ? errors?.password?.message : ""}
                />
                <Button
                    theme="primary"
                    type="submit"
                    label="Login"
                />
            </form>
        </div>
    )
}

export default Login