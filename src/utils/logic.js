import { useContext } from "react"
import { loginContext } from "../context/LoginContext"
export const Logged = () => {
    // const { loggedin } = useContext(loginContext)
    // return loggedin
    const login = localStorage.getItem("token")
    return login
}

export const topics = () => {
    let topics_covered = {
        useFormHook: 40,
        Bootstrap: 23,
        Context: 23,
        ReduxToolKit: 23,
        UseMemo: 78,
        HOC: 23,
        Refs: 12,
        LazyLoading: 22,
        Apiintegration: 22,
        Routing: 45,
        ConditionalRendering: 23
        // topic:[
        //     "useFormHook", "Bootstrap", "Context API", "Redux Tool Kit", "UseMemo", "UseCallbacks", "HOC", "Refs", "LazyLoading", "Apiintegration", "Routing", "ConditionalRendering"
        // ],

    }
    return topics_covered
}