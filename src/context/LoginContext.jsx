import React, { useState } from 'react'
import { createContext } from 'react'
export const loginContext = createContext()
export const LoginProvider = ({ children }) => {
    const [loggedin, setLoggedIn] = useState(false)
    const [show, setShow] = useState(false)
    const toggle = () => {
        setLoggedIn(!loggedin)
    }
    const toggleShow = () => {
        setShow(!show)
    }
    return (
        <>
            <loginContext.Provider value={{ loggedin, toggle, show, toggleShow }}>
                {children}
            </loginContext.Provider>
        </>
    )
}